<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(App\Alert::class, function (Faker\Generator $faker) {
    return [
        'lat' => $faker->unique()->latitude(-7.93, -8.13),
        'lng' => $faker->unique()->longitude(-35.00, -34.870),
        'image' => $faker->randomElement(['1.png', '2.png', '3.png'])
    ];
});