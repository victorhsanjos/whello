<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = [
        'lat',
        'lng',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
