<?php

namespace App\Http\Controllers;

use App\Alert;

class AlertsController extends Controller
{
    public function index()
    {
        return ['data' => Alert::all()->toArray()];
    }
}
