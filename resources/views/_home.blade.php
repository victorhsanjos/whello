<!DOCTYPE html>
<html>
<head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
<div id="floating-panel">
    <input id="pac-input" class="controls" type="text" placeholder="Digite seu destino">
</div>
<div id="map"></div>
<script>
    var map;
    var markers = [];
    var start_position = {lat: -8.059618, lng: -34.872105};
    var end_position = null

    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        map = new google.maps.Map(document.getElementById('map'), {
            center: start_position,
            zoom: 15
        });

        directionsDisplay.setMap(map);

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
            addMarker(event.latLng);
        });

        var onChangeHandler = function() {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
            end_position = autocomplete.getPlace().geometry.location;

            calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markers.push(marker);

        // Remove marker
        marker.addListener("dblclick", function() {
            marker.setMap(null);
        });
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
            origin: start_position,
            destination: end_position,
            travelMode: 'WALKING',
            provideRouteAlternatives: true,
        }, function(response, status) {
            if (status === 'OK') {
                for (var i = 0, len = response.routes.length; i < len; i++) {
                    console.log(response.routes[i])
                    new google.maps.DirectionsRenderer({
                        map: map,
                        directions: response,
                        routeIndex: i
                    });
                }
                //directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?region=br&language=pt-BR&
key=AIzaSyAVDovi52ND67axsw8Zbcu9zqMJWyV2ytw&callback=initMap&libraries=places"
        async defer></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        setTimeout(function () {
            getAlertsMarkers();
        }, 4000);

        function getAlertsMarkers() {
            $.get("/alerts", function (response) {
                $.each(response.data, function (key, data) {
                    var latLng = new google.maps.LatLng(data.lat, data.lng);
                    // Creating a marker and putting it on the map
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Teste'
                    });
                    marker.setMap(map);
                });
            });
        }
    });
</script>
</body>
</html>
