<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css"
          media="screen,projection"/>
    <style>
        #map-canvas {
            width: 100%;
            height: 400px;
            margin: 0;
            padding: 0px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Whello</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="#">Navbar Link</a></li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><a href="#">Navbar Link</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div class="container">
    <div class="section">
        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m6">
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">location_searching</i>
                        <input type="text" id="autocomplete-input" class="autocomplete">
                    </div>
                    <div class="col s12">
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <button id="go" class="btn-floating btn-large waves-effect waves-light red right"><i class="material-icons">directions</i></button>
            <ul class="collapsible">
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">filter_drama</i>
                        First
                    </div>
                </li>
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">place</i>
                        Second
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <a class="waves-effect waves-light btn-small" id="fast" onclick="$('#modal1').modal('close');
            calculateAndDisplayRoute(directionsService, directionsDisplay, 0)">Rápida</a>
            <a class="waves-effect waves-light btn-small" id="safe" onclick="$('#modal1').modal('close');
            calculateAndDisplayRoute(directionsService, directionsDisplay, 1)">Segura</a>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?region=br&language=pt-BR&
key=AIzaSyAVDovi52ND67axsw8Zbcu9zqMJWyV2ytw&callback=initMap&libraries=places"
        async defer></script>
<script>
    $(document).ready(function() {
        $('.modal').modal();

        setTimeout(function () {
            getAlertsMarkers();
        }, 4000);

        function getAlertsMarkers() {
            $.get("/alerts", function (response) {
                $.each(response.data, function (key, data) {
                    var latLng = new google.maps.LatLng(data.lat, data.lng);
                    // Creating a marker and putting it on the map
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Teste'
                    });
                    marker.setMap(map);
                });
            });
        }
    });
</script>

<script>
    var map;
    var markers = [];
    var start_position = {lat: -8.059618, lng: -34.872105};
    var end_position = null

    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: start_position,
            zoom: 16
        });

        var marker = new google.maps.Marker({
            position: start_position,
            map: map,
            title: 'Hello World!'
        });

        directionsDisplay.setMap(map);

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
            addMarker(event.latLng);
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('autocomplete-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
            end_position = autocomplete.getPlace().geometry.location;

            $('#modal2').modal('open');
        });
    }

    $('#go').click(function () {
        $('#modal2').modal('close');
        $('#modal1').modal('open');

        $('#fast').click(function () {
            $('#modal1').modal('close');
            calculateAndDisplayRoute(directionsService, directionsDisplay, 0);
        });

        $('#safe').click(function () {
            $('#modal1').modal('close');
            calculateAndDisplayRoute(directionsService, directionsDisplay, 1);
        });
    });

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markers.push(marker);

        // Remove marker
        marker.addListener("dblclick", function() {
            marker.setMap(null);
        });
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay, i) {
        directionsService.route({
            origin: start_position,
            destination: end_position,
            travelMode: 'WALKING',
            provideRouteAlternatives: true,
        }, function(response, status) {
            if (status === 'OK') {

                new google.maps.DirectionsRenderer({
                    map: map,
                    directions: response,
                    routeIndex: i
                });

                //directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<!--JavaScript at end of body for optimized loading-->
</body>
</html>